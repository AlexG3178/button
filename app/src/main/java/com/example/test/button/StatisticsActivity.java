package com.example.test.button;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class StatisticsActivity extends AppCompatActivity implements View.OnClickListener {

    final String TAG = "#######";

    public ArrayList<User> data = new ArrayList<>();

    RecyclerView rv_statistics;
    RecyclerView.Adapter adapter;
    RecyclerView.LayoutManager layoutManager;

    Button btn_back;

    String URL = "https://button-ed47.restdb.io/";

    Gson gson = new GsonBuilder().create();
    Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(URL)
            .build();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.statistics);

        rv_statistics = (RecyclerView) findViewById(R.id.rv_statistics);
        rv_statistics.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        rv_statistics.setLayoutManager(layoutManager);

        btn_back = (Button) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        RestdbApi restdbApi = retrofit.create(RestdbApi.class);
        Call<List<User>> call = restdbApi.getAllUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {

                List<User> e = response.body();

                for (int i = 0; i < e.size(); i++) {
                    data.add(new User(e.get(i)._id, e.get(i).name, e.get(i).phone, e.get(i).clicks));
                }
                System.out.println(TAG + (data.get(1)).name);
            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                t.printStackTrace();
            }
        });

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        adapter = new AdapterStaristics(data);
        rv_statistics.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                Intent int_back = new Intent(this, MainActivity.class);
                startActivity(int_back);
                break;
        }
    }

}