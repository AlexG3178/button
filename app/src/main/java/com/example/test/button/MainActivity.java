package com.example.test.button;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView tv_counter;
    DialogFragment df;
    final String TAG = "######";

    String URL = "https://button-ed47.restdb.io/";
    Gson gson = new GsonBuilder().create();
    Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(URL)
            .build();

    ArrayList<User> users = new ArrayList<>();
    Boolean numberExist = false;

    String _id;
    int clicks;
    String name;
    String phone;
    String phoneDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(TELEPHONY_SERVICE);
        phoneDevice = telephonyManager.getLine1Number();
        System.out.println(TAG + phoneDevice);

        tv_counter = (TextView) findViewById(R.id.tv_counter);
        Button btn_button = (Button) findViewById(R.id.btn_button);
        Button btn_statistics = (Button) findViewById(R.id.btn_statistics);

        btn_button.setOnClickListener(this);
        btn_statistics.setOnClickListener(this);

        getUsers();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < users.size(); i++) {
            if ((Integer.toString(users.get(i).phone)).equals(phoneDevice)) {
                numberExist = true;
                _id = users.get(i)._id;
                name = users.get(i).name;
                phone = Integer.toString(users.get(i).phone);
                clicks = users.get(i).clicks;
                break;
            }
        }

        if (!numberExist) {
            df = new LoginFragment();
            df.show(getFragmentManager(), "df");

            getUsers();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < users.size(); i++) {
                if ((Integer.toString(users.get(i).phone)).equals(phoneDevice)) {
                    numberExist = true;
                    _id = users.get(i)._id;
                    name = users.get(i).name;
                    phone = Integer.toString(users.get(i).phone);
                    clicks = users.get(i).clicks;
                    break;
                }
            }

            tv_counter.setText(clicks);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_button:
                clicks += 1;
                String cnt = Integer.toString(clicks);
                tv_counter.setText(cnt);
                break;
            case R.id.btn_statistics:
                saveCount();
                Intent int_statistics = new Intent(this, StatisticsActivity.class);
                startActivity(int_statistics);
                break;

        }
    }

    private void getUsers() {
        RestdbApi restdbApi = retrofit.create(RestdbApi.class);
        Call<List<User>> call = restdbApi.getAllUsers();
        call.enqueue(new Callback<List<User>>() {
            @Override
            public void onResponse(Call<List<User>> call, Response<List<User>> response) {
                List<User> e = response.body();
                for (int i = 0; i < e.size(); i++) {
                    users.add(new User(e.get(i)._id, e.get(i).name, e.get(i).phone, e.get(i).clicks));
                }
            }
            @Override
            public void onFailure(Call<List<User>> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private void saveCount() {
        String clickCount = Integer.toString(clicks);

        Map<String, String> updatedUser = new HashMap<>();
        updatedUser.put("name", name);
        updatedUser.put("phone", phone);
        updatedUser.put("clicks", clickCount);

        RestdbApi restdbApi = retrofit.create(RestdbApi.class);
        Call<Object> call = restdbApi.putCountToDB(_id, updatedUser);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveCount();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveCount();
    }
}