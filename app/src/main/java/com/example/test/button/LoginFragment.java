package com.example.test.button;

import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.test.button.R.id.btn_login;


public class LoginFragment extends DialogFragment implements View.OnClickListener {

    final String TAG = "#########";
    String URL = "https://button-ed47.restdb.io/";
    Gson gson = new GsonBuilder().create();
    Retrofit retrofit = new Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(URL)
            .build();

    String login;
    EditText text;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getDialog().setTitle("Please enter your name");
        View v = inflater.inflate(R.layout.login_fragment, null);
        v.findViewById(btn_login).setOnClickListener(this);

        text = (EditText) v.findViewById(R.id.et_login);

        return v;
    }

    @Override
    public void onClick(View v) {

        login = text.getText().toString();
        if(!login.isEmpty()) {

        Map<String, String> map = new HashMap<>();
        map.put("name", login);
        map.put("phone", "507775555");
        map.put("clicks", "0");

        RestdbApi restdbApi = retrofit.create(RestdbApi.class);
        Call<Object> call = restdbApi.addUser(map);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
            }
            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                t.printStackTrace();
            }
        });
            dismiss();
        }else{
            Toast.makeText(getActivity(), "Please enter your name!", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        Log.d(TAG, "Dialog 1: onDissmiss");
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        super.onCancel(dialog);
        Log.d(TAG, "Dialog 1: onCancel");
    }
}
