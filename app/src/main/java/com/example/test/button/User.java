package com.example.test.button;

class User {
    String _id;
    public String name;
    int phone;
    int clicks;

    User(String _id, String name, int phone, int clicks) {
        this._id = _id;
        this.name = name;
        this.phone = phone;
        this.clicks = clicks;
    }
}