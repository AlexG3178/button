package com.example.test.button;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

class AdapterStaristics extends RecyclerView.Adapter<AdapterStaristics.ViewHolder>{
    static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv_statistics_name, tv_statistics_clicks;

        ViewHolder(View view) {
            super(view);
            tv_statistics_name = (TextView) view.findViewById(R.id.tv_statistics_name);
            tv_statistics_clicks = (TextView) view.findViewById(R.id.tv_statistics_clicks);
        }
    }

    private ArrayList<User> data;

     AdapterStaristics(ArrayList<User> data) {
        this.data = data;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public AdapterStaristics.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.statistic_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.tv_statistics_name.setText(data.get(position).name);
        holder.tv_statistics_clicks.setText(Integer.toString(data.get(position).clicks));
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }
}

