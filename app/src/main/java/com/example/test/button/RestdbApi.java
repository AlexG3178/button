package com.example.test.button;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

interface RestdbApi {

    @Headers("x-apikey: 32770eaca817b017cf400417713303aed4981")
    @GET("rest/buttoncollection")
    Call<List<User>> getAllUsers ();

    @Headers("x-apikey: 32770eaca817b017cf400417713303aed4981")
    @POST("rest/buttoncollection")
    Call<Object> addUser(@Body Object user);

    @Headers("x-apikey: 32770eaca817b017cf400417713303aed4981")
    @Multipart
    @PUT("rest/buttoncollection/{_id}")
    Call<Object> putCountToDB(@Path("_id") String _id, @Body Object updatedUser);
}
